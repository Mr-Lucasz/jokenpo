## BUG SCENARIO

**Criteria: LOG IN AS ADMIN**

STEPS FOR REPRODUCTION:

1. STEP 1
2. STEP 2
3. STEP 3
4. STEP 4
5. STEP 5
6. STEP 6

Result obtained:

**Here you will write important details about the BUG, be detailed and clear when reporting**

ATTACHMENT: PHOTO OR VIDEO
OR BOTH, SHOWING THE BUG FOUND
![image](/uploads/6f76f567af937b3c7fcefc6ffaa9416d/image.png)

Expected result:

Describe what would be the correct behavior of the functionality according to the established system requirements. You can use prototypes!
